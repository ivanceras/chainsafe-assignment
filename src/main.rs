use ipfs_api::IpfsClient;
use std::error::Error;
use std::fs::File;
use std::path::PathBuf;
use structopt::StructOpt;

use keyring::AccountKeyring;
use mushroom::{compose_extrinsic, rpc::WsRpcClient, Api, UncheckedExtrinsicV4, XtStatus};
use sp_core::crypto::Pair;

#[derive(Debug, StructOpt)]
#[structopt(name = "ipfs files", about = "ipfs file upload")]
struct Opt {
    #[structopt(name = "FILE")]
    file_name: String,
}

/// Upload the file to ipfs and return the it's filename used in the ipfs
/// This will then be out CID for the next step.
async fn ipfs_upload(file: File) -> Result<String, anyhow::Error> {
    eprintln!("connecting to (default) localhost:5001...");
    let client = IpfsClient::default();
    match client.add(file).await {
        Ok(file) => Ok(file.name),
        Err(e) => anyhow::bail!("error uploading file to ipfs: {}\nMake sure you issued the command `ipfs daemon` first", e),
    }
}

/// Store the CID into out IpfsCid module in our substrate node.
fn store_cid_to_substrate_node(cid: &str) -> Result<(), anyhow::Error> {
    let url = "ws://127.0.0.1:9944";

    let from = AccountKeyring::Alice.pair();
    let client = WsRpcClient::new(&url);
    let api = Api::new(client).map(|api| api.set_signer(from))
        .unwrap_or_else(|e|panic!("Unable to connect to substrate-node-template: {:?}
                \nMake sure the substrate-node-template is running
                \nIssue the command in another terminal: `cd examples/substrate-node-template &&./start_node.sh`
                \n
                ",e));

    let xt: UncheckedExtrinsicV4<_> =
        compose_extrinsic!(api.clone(), "IpfsCid", "store_cid", cid.as_bytes().to_vec());

    println!("[+] Composed Extrinsic:\n {:?}\n", xt);

    // send and watch extrinsic until InBlock
    let tx_hash = api
        .send_extrinsic(xt.hex_encode(), XtStatus::InBlock)
        .expect("must send the extrinsic");
    println!("[+] Transaction got included. Hash: {:?}", tx_hash);
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    eprintln!("note: this must be run in the root of the project repository");
    let opt = Opt::from_args();
    println!("{:?}", opt);

    let file = File::open(opt.file_name).expect("could not read source file");
    let cid = ipfs_upload(file).await?;
    store_cid_to_substrate_node(&cid)?;
    println!("CID: {}", cid);
    Ok(())
}

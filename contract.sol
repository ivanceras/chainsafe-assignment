// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

/**
 * @title Storage
 * @dev Store & retrieve value in a variable
 */
contract Storage {

    string cid;

    /**
     * @dev Store value in variable
     * @param arg value to store
     */
    function store(string memory arg) public {
        cid = arg;
    }

    /**
     * @dev Return value 
     * @return value of 'cid'
     */
    function retrieve() public view returns (string memory){
        return cid;
    }
}

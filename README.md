# README

This code will take a FILE as parameter and upload it to `ipfs`.
The `CID` returned from `ipfs` will be stored into a substrate node.

This codebase include a bare fork of `substrate-node-template`, a pallet `ipfs-cid` is added
to store the `CID` of the file uploaded to `ipfs`

## Prerequisites

Substrate uses nightly compiler of rust.
Finding the compatible version will be a bit tricky at the moment.

I've found `nightly-2021-05-20` is able to compile the tagged version(`monthly-2021-07`) of substrate used in this codebase.

```sh
rustup toolchain add nightly-2021-05-20
rustup target add wasm32-unknown-unknown
```

## Background services

Both ipfs and the substrate node would need to be running beforehand.

Run the ifps daemon. This needs to be running in a different terminal
Install [ifps](https://ipfs.io/#install)
```sh
ipfs init
ipfs daemon
```
Open a new terminal for the next step

Run the included `substrate-node-template` code in `examples/substrate-node-template`
```sh
cd examples/substrate-node-template
./start_node.sh
```

Open a new terminal for the next step

## Build and run the app

```sh
cargo build --release
```

Running and uploading the README file to ipfs:
```sh
./target/release/chainsafe-assignment README.md
```
Alternatively, this could also be done using
```sh
cargo run --release -- README.md
```

[Polkadotjs extension](https://polkadot.js.org/extension/) for the browser is needed to be installed.
Verify that the CID is stored in our chain using https://polkadot.js.org/apps/
Connect the polkadotjs client to localnode ws://127.0.0.1:9944

Interact with the `IpfsCid` module in `Developer` > `Chainstate` to query the current CID stored.
You can also Interact with `IpfsCid` module in `Developer` > `Extrinsics` and use the `storeCid` to store a CID manually.


To test out the result:
We will read the CID stored in our `ipfs-cid` pallet in substrate node and then retriving the file from ipfs.

```sh
cargo run --bin retrieve_stored_file
```



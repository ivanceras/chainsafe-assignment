FROM debian:buster-slim

RUN apt-get update 

RUN apt install curl -y

ENV rust_toolchain  nightly-2021-03-25

ADD ./dockerfile.d/install-rust.sh /root
RUN bash /root/install-rust.sh
RUN /root/.cargo/bin/rustup install ${rust_toolchain}
RUN /root/.cargo/bin/rustup default ${rust_toolchain}
RUN /root/.cargo/bin/rustup target add wasm32-unknown-unknown --toolchain ${rust_toolchain}
ENV PATH=$PATH:$HOME/.cargo/bin

COPY ./examples/substrate-node-template /root/

WORKDIR /root/substrate-node-template/

CMD /root/subtrate-node-template/start_node.sh

EXPOSE 9944

use keyring::AccountKeyring;
use mushroom::{compose_extrinsic, rpc::WsRpcClient, Api, UncheckedExtrinsicV4, XtStatus};
use sp_core::crypto::Pair;

fn main() {
    let cid_value = "JustANyValueWegetFromIpfsUpload";
    let url = "ws://127.0.0.1:9944";

    let from = AccountKeyring::Alice.pair();
    let client = WsRpcClient::new(&url);
    let api = Api::new(client).map(|api| api.set_signer(from)).unwrap();

    let cid_bytes: Vec<u8> = cid_value.as_bytes().to_vec();

    #[allow(clippy::redundant_clone)]
    let xt: UncheckedExtrinsicV4<_> =
        compose_extrinsic!(api.clone(), "IpfsCid", "store_cid", cid_bytes);

    println!("[+] Composed Extrinsic:\n {:?}\n", xt);

    // send and watch extrinsic until InBlock
    let tx_hash = api
        .send_extrinsic(xt.hex_encode(), XtStatus::InBlock)
        .unwrap();
    println!("[+] Transaction got included. Hash: {:?}", tx_hash);
}

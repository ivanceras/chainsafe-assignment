use crate::{mock::*, Error};
use frame_support::{assert_noop, assert_ok};

#[test]
fn it_works_for_default_value() {
	new_test_ext().execute_with(|| {
		// Dispatch a signed extrinsic.
		assert_ok!(IpfsCid::store_cid(Origin::signed(1), b"store_this_one".to_vec()));
		// Read pallet storage and assert an expected result.
		assert_eq!(IpfsCid::cid(), Some(b"store_this_one".to_vec()));
	});
}

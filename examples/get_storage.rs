use futures::TryStreamExt;
use ipfs_api::IpfsClient;
use keyring::AccountKeyring;
use mushroom::{rpc::WsRpcClient, Api, UncheckedExtrinsicV4, XtStatus};
use sp_core::crypto::Pair;
use std::io;
use std::io::Write;

/// This example get the cid from our substrate node
/// and then use that cid to retrieve the file from ipfs
#[tokio::main]
pub async fn main() {
    let url = "ws://127.0.0.1:9944";

    let from = AccountKeyring::Alice.pair();
    let client = WsRpcClient::new(&url);
    let api = Api::new(client).map(|api| api.set_signer(from)).unwrap();

    // get the cid store,
    let result: Option<Vec<u8>> = api.get_storage_value("IpfsCid", "CidStore", None).unwrap();
    // CidStore is using `Vec<u8>` for out datatype since `String` is not supported in substrate.
    // We convert the bytes into utf8 here:
    let cid = result.map(|r| std::str::from_utf8(&r).unwrap().to_string());
    println!("Stored into the node: {:?}", cid);

    if let Some(cid) = cid {
        println!("Now getting the file from ipfs: {}", cid);
        let client = IpfsClient::default();
        match client
            .get(&cid)
            .map_ok(|chunk| chunk.to_vec())
            .try_concat()
            .await
        {
            Ok(res) => {
                let out = io::stdout();
                let mut out = out.lock();

                out.write_all(&res).unwrap();
            }
            Err(e) => eprintln!("error getting file: {}", e),
        }
    }
}

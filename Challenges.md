# Challenges

- Limited experience with ethereum and solidity.
    - Was able to create a run and execute contract in remix IDE.
    - I wasn't able to do uploading ethereum contracts and calling it automatically in scripts.
    - ganache and truffle seems to work fine, but my `npm/node js` stack is very clanky and breaks all the time.

- Finding the correct tagged version in substrate and the compatible nightly compiler.
    - backtrack nighly compilers which can work with the tagged version of substrate.
- outdated version of tools such as binaryen where substrate needs to use a more recent release.
    - To accomplish this, we need to compile the code of binaryen and install locally.
- Smart contracts and `ink` in substrate is not widely used as pallets.
    - Had trouble uploading and running the ink contract.

